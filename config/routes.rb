Rails.application.routes.draw do

  #root to: "people#index"
  post 'authenticate', to: 'authentication#authenticate'

  	
	resources :library do
    	collection do
      	post :fetch_library
      	post :create_date
      
    	end
	end

end