# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'faker'
include Faker
5.times do
Author.create(
name: Faker::Name.name,
bio: Faker::Name.bio,
profile_pic: Faker::Avatar.image("my-own-slug", "50x50", "jpg"),
academics: Faker::Name.academics,
awards: Faker::Name.awards
	)	
end
