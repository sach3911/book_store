class BaseController < ActionController::Base
  before_action :require_login!
  helper_method :user_signed_in?, :current_user

  def user_signed_in?
    current_user.present?
  end

  def require_login!
    return true if authenticate_token
    render json: { errors: [ { detail: "Access denied" } ] }, status: 401
  end

  def current_user
    @_current_user ||= authenticate_token
  end

  private
    def authenticate_token
      authenticate_with_http_token do |token, options|
       
        #User.find_by(authentication_token: token)
        if params[:organization_id]
          logger.info(params[:organization_id])
          User.where(authentication_token: token).where("token_created_at >= ? and ou.organization_id=? and ou.deactivated=0", 
          1.month.ago ,params[:organization_id]).joins('LEFT JOIN organization_users ou ON ou.user_id=users.id').first 
          
        else
          logger.info("no org id")
          User.where(authentication_token: token).where("token_created_at >= ?", 1.month.ago).first
        
        end
             
      end
      
      
    end

end