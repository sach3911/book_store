class AuthenticationController < ApplicationController
	before_action :require_login!, except:[:authenticate]
 skip_before_action :authenticate_request, :only => :authenticate 
 skip_before_filter :verify_authenticity_token, :only => :authenticate 

 def authenticate 
 	puts params
 	user = User.find_by_email_pass(params[:email],params[:password]) 
 	if !user.nil? 

 		user.generate_authentication_token
         # render json:user.as_json , status: 200
          render json: { user: user.attributes.except('password'), status: 200, token: user.authentication_token },
               status: 200
 		
 	else 
 		render json: { error: "Wrong" }, status: :unauthorized 
 	end 
 end 
end
