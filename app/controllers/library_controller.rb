
class LibraryController < ApplicationController

	
	include Faker

	def fetch_library
	  #fetch data for Review start
	  @review = []
		@final_review = []
		@review_name = Review.fetch_by_name_of_reviewer(params[:search_key])
		@review_title = Review.fetch_by_title(params[:search_key])
		@review_description = Review.fetch_by_description(params[:search_key])
		@review_book_name = Review.fetch_by_book_name(params[:search_key])

			@review_name.each do |r_name|

				@review.push(r_name)			
			end

			@review_description.each do |desc|

				@review.push(desc)			
			end	

			@review_book_name.each do |b_name|

				@review.push(b_name)			
			end
	 
		 	@review_title.each do |title|
		 		@review.push(title)
		 		puts "kkkkk"		 	
		 	end


      if @review.length >0
        @final_review.push(@review.first)
  		 	@review.each do |r1|
  		 		flag = 0
  		 		@final_review.each do |r2|
  
  			 		if r1[:_id] == r2[:_id]
  			 			flag = 1
  			 		end
  			 	end
  			 	if flag == 0
  			 		@final_review.push(r1)
  			 	end
  		 	end
  		end
      
  #======================fetch data for Review start
  
  #=======================fetch data for Book start
  
    @books = []
    @final_books = []
    @book_name = MyBook.fetch_by_bookName(params[:search_key])
    @book_short_description = MyBook.fetch_by_short_description(params[:search_key])
    @book_long_description = MyBook.fetch_by_description(params[:search_key])
    @book_author_name = MyBook.fetch_by_author_name(params[:search_key])

      @book_name.each do |b_name|

        @books.push(b_name)      
      end

      @book_short_description.each do |desc|

        @books.push(desc)      
      end 

      @book_long_description.each do |b_dec|

        @books.push(b_dec)      
      end
   
      @book_author_name.each do |a_name|
        @books.push(a_name)
        puts "kkkkk"      
      end
      
      if @books.length > 0
      @final_books.push(@books.first)
      @books.each do |b1|
        puts b1[:_id]
        b_flag = 0
        @final_books.each do |b2|
          if b1[:_id] == b2[:_id]
            
            b_flag = 1
            puts "t"
          else
            puts "f"
          end
        end
        if b_flag == 0
          puts "mmmmmmmmmmmmmmm"
          @final_books.push(b1)
        end
      end
  end
  
   #=======================fetch data for Book end
  
  #=======================fetch data for Author start
  
    @authors = []
    @final_authors = []
    @authors_name = Author.fetch_by_authorName(params[:search_key])
    @authors_bio = Author.fetch_by_bio(params[:search_key])
    @authors_academics = Author.fetch_by_academics(params[:search_key])

      @authors_name.each do |b_name|

        @authors.push(b_name)      
      end

      @authors_bio.each do |desc|

        @authors.push(desc)      
      end 

      @authors_academics.each do |b_dec|

        @authors.push(b_dec)      
      end
   
      if @authors.length > 0
      @final_authors.push(@authors.first)
      @authors.each do |a1|
        a_flag = 0
        @final_authors.each do |a2|
          if a1[:_id] == a2[:_id]
            
            a_flag = 1
            puts "t"
          else
            puts "f"
          end
        end
        if a_flag == 0
          puts "mmmmmmmmmmmmmmm"
          @final_authors.push(a1)
        end
      end
  end
  
   
		respond_to do |format|
        	format.json { render json: {books: @final_books,authors:@final_authors, reviews: @review}}
    	end 
	end

	def create_date



		#5000.times do
			
		#	MyBook.create(
		#	name: Faker::Book.title  ,
		#	author_name: Faker::Book.author,
		#	short_description: Faker::Hipster.sentence(3),
		#	long_description: Faker::Hipster.sentences,
		#	chapter_index: Faker::Number.number(10),
		#	date_of_publication: Faker::Date.forward(),
		#	genre: Faker::Book.genre
		#	)	
		#end


		#1000.times do
			
		#	Author.create(
		#	name: Faker::Name.name  ,
		#	bio: Faker::Lorem.paragraph(10, false, 100),
		#	profile_pic: Faker::Avatar.image,
		#	academics: Faker::Educator.course,
		#	awards: Faker::Lorem.words
		#		)	
		#end



		25000.times do
			
			Review.create(
			name_of_reviewer: Faker::Name.name  ,
			rating: Faker::Number.between(1, 5),
			title: Faker::Hipster.sentence(1),
			description: Faker::Hipster.sentence(5),
			book_name: Faker::Book.title
				)	
		end



	respond_to do |format|
        format.json { render json: "success" }
    end 

	end
end
