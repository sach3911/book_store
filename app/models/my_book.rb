class MyBook
  include Mongoid::Document
  db = Mongoid::Sessions.default
  collection = db[:my_books]
  Mongoid::Attributes::Dynamic
  field :name, type: String
  field :short_description, type: String
  field :long_description, type: String
  field :chapter_index, type: String
  field :date_of_publication, type: Date
  field :genre, type: String
  field :author_name, type: String
  
   
  def self.fetch_by_bookName(searchterm)
  collection.find(name: /#{searchterm}/)  

end

def self.fetch_by_short_description(searchterm)
  collection.find(short_description: /#{searchterm}/) 

end

def self.fetch_by_description(searchterm)
  collection.find(long_description: /#{searchterm}/) 

end

def self.fetch_by_author_name(searchterm)
  collection.find(author_name: /#{searchterm}/) 

end
  
  
  
end
