class Author
  include Mongoid::Document
  db = Mongoid::Sessions.default
  collection = db[:authors]
  Mongoid::Attributes::Dynamic
  field :name, type: String
  field :bio, type: String
  field :profile_pic, type: String
  field :academics, type: String
  field :awards, type: String


  def self.fetch_by_authorName(searchterm)
    collection.find(name: /#{searchterm}/)  
  
  end
  
  def self.fetch_by_bio(searchterm)
    collection.find(bio: /#{searchterm}/) 
  
  end
  
  def self.fetch_by_academics(searchterm)
    collection.find(academics: /#{searchterm}/) 
  
  end


end


