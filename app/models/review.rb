class Review
  include Mongoid::Document
  db = Mongoid::Sessions.default
  collection = db[:reviews]
  Mongoid::Attributes::Dynamic
  field :name_of_reviewer, type: String
  field :rating, type: String
  field :title, type: String
  field :description, type: String
  field :book_name, type: String

def self.fetch_by_name_of_reviewer(searchterm)
	collection.find(name_of_reviewer: /#{searchterm}/)	

end

def self.fetch_by_title(searchterm)
	collection.find(title: /#{searchterm}/)	

end

def self.fetch_by_description(searchterm)
	collection.find(description: /#{searchterm}/)	

end

def self.fetch_by_book_name(searchterm)
	collection.find(book_name: /#{searchterm}/)	

end

end
