class Book
  include Mongoid::Document
  field :name, type: String
  field :short_description, type: String
  field :long_description, type: String
  field :chapter_index, type: Number
  field :date_of_publication, type: Date
  field :genre, type: Array
  field :author_name, type: String
end
