class User
	#has_secure_password
  include Mongoid::Document
  db = Mongoid::Sessions.default
  collection = db[:users]
  Mongoid::Attributes::Dynamic
  field :name, type: String
  field :email, type: String
  field :password, type: String
  field :authenticate_token, type:String

def self.find_by_email_pass(email,pass)
collection.find(email: email).first
end


  def generate_authentication_token
    loop do
      self.authentication_token = SecureRandom.base64(64)
     
      break unless User.find_by(authentication_token: authentication_token)
    end
    self.update_columns(authentication_token: authentication_token, token_created_at: Time.zone.now)
  end
end
